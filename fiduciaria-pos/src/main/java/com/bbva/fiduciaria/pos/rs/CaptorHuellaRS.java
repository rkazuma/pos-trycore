package com.bbva.fiduciaria.pos.rs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.bbva.fiduciaria.pos.exception.FiduciariaPOSException;
import com.bbva.fiduciaria.pos.exception.SupremaDeviceException;

@Path("/captor-huellas")
public interface CaptorHuellaRS {
	@GET
	@Path("/iniciar")
	public void iniciar() throws FiduciariaPOSException, SupremaDeviceException;

	@GET
	@Path("/capturar")
	public void capturar() throws FiduciariaPOSException, SupremaDeviceException;

	@GET
	@Path("/template")
	public Response obtenerTemplate() throws FiduciariaPOSException, SupremaDeviceException;

	@GET
	@Path("/calidad")
	public int obtenerCalidad() throws FiduciariaPOSException, SupremaDeviceException;

	@GET
	@Path("/verificar")
	public void verificarCaptura() throws FiduciariaPOSException, SupremaDeviceException;
}
