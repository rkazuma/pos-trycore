package com.bbva.fiduciaria.pos.rs.impl;

import static com.bbva.fiduciaria.pos.transversales.Constantes.ERROR_CAPTURA_HUELLAS_DIGITALES;
import static com.bbva.fiduciaria.pos.transversales.Constantes.ERROR_INICIALIZACION_CAPTOR_HUELLAS;
import static com.bbva.fiduciaria.pos.transversales.Constantes.ERROR_OBTENCION_CALIDAD_CAPTURA_HUELLA_DIGITAL;
import static com.bbva.fiduciaria.pos.transversales.Constantes.ERROR_OBTENCION_TEMPLATE_HUELLA_DIGITAL;
import static com.bbva.fiduciaria.pos.transversales.Constantes.ERROR_VERIFICACION_CAPTURA_HUELLA_DIGITAL;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.MessageContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import com.bbva.fiduciaria.pos.dto.TokenCaptorDTO;
import com.bbva.fiduciaria.pos.exception.FiduciariaPOSException;
import com.bbva.fiduciaria.pos.exception.SupremaDeviceException;
import com.bbva.fiduciaria.pos.interfaces.BioMiniInterface;
import com.bbva.fiduciaria.pos.rs.CaptorHuellaRS;
import com.bbva.fiduciaria.pos.util.POSUtils;

@Service("captorHuellaRS")
public class CaptorHuellaRSImpl implements CaptorHuellaRS {

	private static final Logger LOG = Logger.getLogger(CaptorHuellaRSImpl.class.getName());

	@Context
	private MessageContext context;

	@Autowired
	private BioMiniInterface bioMiniInterface;

	@Override
	public void iniciar() throws FiduciariaPOSException, SupremaDeviceException {
		try {
			bioMiniInterface.init();
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
			throw new FiduciariaPOSException(ERROR_CAPTURA_HUELLAS_DIGITALES, ERROR_CAPTURA_HUELLAS_DIGITALES);
		}
	}

	@Override
	public void capturar() throws FiduciariaPOSException, SupremaDeviceException {
		try {
			bioMiniInterface.capture();
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
			throw new FiduciariaPOSException(ERROR_INICIALIZACION_CAPTOR_HUELLAS, ERROR_INICIALIZACION_CAPTOR_HUELLAS);
		}
	}

	@Override
	public Response obtenerTemplate() throws FiduciariaPOSException, SupremaDeviceException {
		try {
			TokenCaptorDTO tokenCaptorObject = (TokenCaptorDTO) context.getHttpServletRequest()
					.getAttribute(POSUtils.TOKEN_CAPTOR_ATTRIBUTE_KEY);
			byte[] cKeyBytes = Base64Utils.decodeFromString(tokenCaptorObject.getCkey());
			byte[] bytesTemplate = bioMiniInterface.getEncryptedTemplate(cKeyBytes);
			return Response.ok(Base64Utils.encodeToString(bytesTemplate), MediaType.TEXT_PLAIN).build();
		} catch (SupremaDeviceException e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
			throw new FiduciariaPOSException(ERROR_OBTENCION_TEMPLATE_HUELLA_DIGITAL,
					ERROR_OBTENCION_TEMPLATE_HUELLA_DIGITAL);
		}
	}

	@Override
	public int obtenerCalidad() throws FiduciariaPOSException, SupremaDeviceException {
		try {
			return bioMiniInterface.getTemplateQuality();
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
			throw new FiduciariaPOSException(ERROR_OBTENCION_CALIDAD_CAPTURA_HUELLA_DIGITAL,
					ERROR_OBTENCION_CALIDAD_CAPTURA_HUELLA_DIGITAL);
		}
	}

	@Override
	public void verificarCaptura() throws FiduciariaPOSException, SupremaDeviceException {
		try {
			bioMiniInterface.verify();
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
			throw new FiduciariaPOSException(ERROR_VERIFICACION_CAPTURA_HUELLA_DIGITAL,
					ERROR_VERIFICACION_CAPTURA_HUELLA_DIGITAL);
		}
	}
}
