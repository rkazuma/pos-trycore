package com.bbva.fiduciaria.pos.interfaces.impl;

import org.springframework.stereotype.Component;

import com.bbva.fiduciaria.pos.exception.SupremaDeviceException;
import com.bbva.fiduciaria.pos.interfaces.BioMiniInterface;
import com.suprema.BioMiniHidFactory;
import com.suprema.Hid;
import com.suprema.IBioMiniHid;
import com.suprema.IUsbEventHandler;

@Component
public class BioMiniInterfaceImpl implements BioMiniInterface {
	private IBioMiniHid mBioMini = null;

	private BioMiniHidFactory mBioMiniFactory = new BioMiniHidFactory() {
		@Override
		public void onDeviceChange(IUsbEventHandler.DeviceChangeEvent event, Object dev) {
			if (mBioMini != null) {
				if (mBioMini.IsEqual(dev) && event == IUsbEventHandler.DeviceChangeEvent.DEVICE_DETACHED) {
					mBioMini = null;
				}
			}
		}
	};

	public void init() throws SupremaDeviceException {
		mBioMini = mBioMiniFactory.getDevice(0);

		if (mBioMini == null) {
			throw new SupremaDeviceException("No se pudo establecer conexión con el dispositivo");
		}

		// Set configs
		int re = 0;

		// Set timeout
		int capTimeout = 4000;
		re = mBioMini.HidCommand(Hid.Pac.PAC_SET, Hid.Cmd.VAT_CAPTURE_OPT, Hid.Sub.CVT_TIMEOUT,
				Hid.shortToBytes(capTimeout));
		if (re != 0) {
			throw new SupremaDeviceException("Error seteando el tiempo de espera : " + Hid.errString(re));
		}

		// Set LFD
		int lfdLevel = 0;
		re = mBioMini.HidCommand(Hid.Pac.PAC_SET, Hid.Cmd.VAT_CAPTURE_OPT, Hid.Sub.CVT_LFD_LEVEL,
				Hid.byteToBytes(lfdLevel));
		if (re != 0) {
			throw new SupremaDeviceException("Error seteando el nivel LFD : " + Hid.errString(re));
		}

		// Set template type
		int templateType = 2002; // 2001:Suprema, 2002:ISO, 2003:ANSI
		re = mBioMini.HidCommand(Hid.Pac.PAC_SET, Hid.Cmd.VAT_TEMPLATE_OPT, Hid.Sub.TVT_TEMPLATE_FORMAT,
				Hid.shortToBytes(templateType));
		if (re != 0) {
			throw new SupremaDeviceException("Error seteando el tipo de template : " + Hid.errString(re));
		}

		// Set security level
		int securityLevel = 4;
		re = mBioMini.HidCommand(Hid.Pac.PAC_SET, Hid.Cmd.VAT_VERIFIY_OPT, Hid.Sub.VVT_SECURITY_LEVEL,
				Hid.byteToBytes(securityLevel));
		if (re != 0) {
			throw new SupremaDeviceException("Error seteando el nivel de seguridad : " + Hid.errString(re));
		}
	}

	public void capture() throws SupremaDeviceException {
		if (mBioMini == null) {
			throw new SupremaDeviceException("El dispositivo no ha sido inicializado");
		}

		mBioMini.SetCommandTimeout(10000);

		int re = mBioMini.HidCommand(Hid.Pac.PAC_CMD, Hid.Cmd.CMT_CAPTURE, Hid.Sub.CCT_SINGLE, null);

		if (re != 0) {
			throw new SupremaDeviceException("Error capturando la huella : " + Hid.errString(re));
		}
	}

	public byte[] getCapture() throws SupremaDeviceException {
		if (mBioMini == null) {
			throw new SupremaDeviceException("El dispositivo no ha sido inicializado");
		}

		// set image format as RAW
		int re = mBioMini.HidCommand(Hid.Pac.PAC_GET, Hid.Cmd.VAT_IMAGE_OPT, Hid.Sub.IVT_IMAGE_FORMAT, null);
		if (re != 0) {
			throw new SupremaDeviceException("Error formateando la imagen : " + Hid.errString(re));
		}

		byte[] data = mBioMini.ReceiveData(Hid.Sub.DBI_CAPTURED_IMAGE, 0, 0);
		if (data == null) {
			throw new SupremaDeviceException("Error extrayendo la captura");
		}

		return data;
	}

	public byte[] getTemplate() throws SupremaDeviceException {
		if (mBioMini == null) {
			throw new SupremaDeviceException("El dispositivo no ha sido inicializado");
		}

		mBioMini.SetCommandTimeout(2000);

		int re = mBioMini.HidCommand(Hid.Pac.PAC_CMD, Hid.Cmd.CMT_EXTRACT, Hid.Sub.DBI_CAPTURED_TEMPLATE, null);
		if (re != 0) {
			throw new SupremaDeviceException("Error extrayendo el template de la captura : " + Hid.errString(re));
		}

		byte[] data = mBioMini.ReceiveData(Hid.Sub.DBI_CAPTURED_TEMPLATE, 2, 75);
		if (data == null) {
			throw new SupremaDeviceException("Error extrayendo el template de la captura");
		}

		return data;
	}

	public byte[] getEncryptedTemplate(byte[] cKey) throws SupremaDeviceException {
		if (mBioMini == null) {
			throw new SupremaDeviceException("El dispositivo no ha sido inicializado");
		}

		mBioMini.SetCommandTimeout(2000);

		int re = 0;

		// Enviando comando al lector para configurar la clave de encriptado,
		// CEM_UNIQ_N_AES = Rindjael AES
		re = mBioMini.HidCommand(Hid.Pac.PAC_SET, Hid.Cmd.VAT_ENCRYPT_OPT, Hid.Sub.CEM_UNIQ_N_AES, cKey);
		if (re != 0) {
			throw new SupremaDeviceException("Error configurando clave de encriptación");
		}

		// Trayendo el template Mediante el subcomando DBI_CAPTURED_TEMPLATE y
		// asignando el array
		byte[] data = mBioMini.ReceiveData(Hid.Sub.DBI_CAPTURED_TEMPLATE_ENC, 2, 75);
		if (data == null) {
			throw new SupremaDeviceException("Error extrayendo el template encriptado de la captura");
		}

		return data;
	}

	public int getTemplateQuality() throws SupremaDeviceException {
		if (mBioMini == null) {
			throw new SupremaDeviceException("El dispositivo no ha sido inicializado");
		}

		// Antes de recuperar la calidad se debe extraer el template si
		// encriptar, esto genera los bytes necesarios para recuperar la calidad
		getTemplate();

		return Hid.retrieveByte(mBioMini.GetHidEchoData(), 4);
	}

	public void verify() throws SupremaDeviceException {
		if (mBioMini == null) {
			throw new SupremaDeviceException("El dispositivo no ha sido inicializado");
		}

		// Antes de verificar se debe enviar el template al dispositivo para que
		// tenga contra que verificar
		sendTemplate();

		mBioMini.SetCommandTimeout(10000);

		int re = mBioMini.HidCommand(Hid.Pac.PAC_CMD, Hid.Cmd.CMT_VERIFY, Hid.Sub.CVM_TRANSFERED_TEMPLATE, null);
		if (re != 0) {
			throw new SupremaDeviceException("Error verificando la huella : " + Hid.errString(re));
		}
	}

	private void sendTemplate() throws SupremaDeviceException {
		int re = mBioMini.SendData(Hid.Sub.CST_ISO_TEMPLATE, getTemplate());
		if (re != 0) {
			throw new SupremaDeviceException("Error enviando el template : " + Hid.errString(re));
		}
	}
}
