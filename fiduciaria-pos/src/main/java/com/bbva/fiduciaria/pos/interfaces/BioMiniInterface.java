package com.bbva.fiduciaria.pos.interfaces;

import com.bbva.fiduciaria.pos.exception.SupremaDeviceException;

public interface BioMiniInterface
{
	public void init() throws SupremaDeviceException;

	public void capture() throws SupremaDeviceException;

	public byte[] getCapture() throws SupremaDeviceException;

	public byte[] getTemplate() throws SupremaDeviceException;

	public byte[] getEncryptedTemplate(byte[] cKey) throws SupremaDeviceException;

	public int getTemplateQuality() throws SupremaDeviceException;

	public void verify() throws SupremaDeviceException;
}
