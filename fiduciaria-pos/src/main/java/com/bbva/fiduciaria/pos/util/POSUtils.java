package com.bbva.fiduciaria.pos.util;

import java.io.InputStream;
import java.util.Properties;

public class POSUtils {

	public static final String TOKEN_CAPTOR_ATTRIBUTE_KEY = "TOKEN_CAPTOR_ATTRIBUTE";

	public static Properties properties;

	public static String getProperty(String key) throws Exception {
		if (properties == null) {
			properties = new Properties();
			InputStream inputStream = POSUtils.class.getClassLoader().getResourceAsStream("config.properties");
			properties.load(inputStream);
		}

		return (String) properties.get(key);
	}
}
