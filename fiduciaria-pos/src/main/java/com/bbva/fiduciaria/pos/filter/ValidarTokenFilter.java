package com.bbva.fiduciaria.pos.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import com.bbva.fiduciaria.pos.dto.TokenCaptorDTO;
import com.bbva.fiduciaria.pos.util.POSUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ValidarTokenFilter implements Filter {

	private static final Logger LOG = Logger.getLogger(ValidarTokenFilter.class.getName());

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest httpReq = (HttpServletRequest) req;
		HttpServletResponse httpRes = (HttpServletResponse) res;

		String methodOption = "";
		String headerUsuarioFiduciaria = "";
		String headerTokenCaptor = "";
		try {
			methodOption = POSUtils.getProperty("fiduciaria.front.http.method.options");
			headerUsuarioFiduciaria = POSUtils.getProperty("fiduciaria.front.http.header.usuariofiduciaria");
			headerTokenCaptor = POSUtils.getProperty("fiduciaria.front.http.header.tokencaptor");
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}

		if (!httpReq.getMethod().equals(methodOption)) {
			String usuarioFiduciaria = httpReq.getHeader(headerUsuarioFiduciaria);
			String tokenCaptor = httpReq.getHeader(headerTokenCaptor);

			if (usuarioFiduciaria == null || tokenCaptor == null) {
				httpRes.sendError(HttpServletResponse.SC_FORBIDDEN, "Falta el token de acceso");
			}

			// Verifico los tokens
			HttpResponse response = verificarToken(usuarioFiduciaria, tokenCaptor);

			if (response.getStatusLine().getStatusCode() != 200) {
				httpRes.sendError(HttpServletResponse.SC_FORBIDDEN, "El token de acceso no es válido");
			}

			TokenCaptorDTO tokenCaptorObject = new ObjectMapper().readValue(response.getEntity().getContent(),
					TokenCaptorDTO.class);
			httpReq.setAttribute(POSUtils.TOKEN_CAPTOR_ATTRIBUTE_KEY, tokenCaptorObject);
		}

		filterChain.doFilter(req, res);
	}

	private HttpResponse verificarToken(String usuarioFiduciaria, String tokenCaptor) throws ServletException {
		try {
			String url = POSUtils.getProperty("fiduciaria.front.token.endpoint");
			String usuarioFiduciariaAuth = POSUtils.getProperty("fiduciaria.front.auth.usuariofiduciaria");
			String tokenCaptorAuth = POSUtils.getProperty("fiduciaria.front.auth.tokencaptor");

			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(url);

			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new BasicNameValuePair(usuarioFiduciariaAuth, usuarioFiduciaria));
			urlParameters.add(new BasicNameValuePair(tokenCaptorAuth, tokenCaptor));
			post.setEntity(new UrlEncodedFormEntity(urlParameters));

			return client.execute(post);
		} catch (Exception e) {
			throw new ServletException(e.getMessage(), e.getCause());
		}
	}
}
