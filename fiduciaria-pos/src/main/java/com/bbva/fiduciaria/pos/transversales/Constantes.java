package com.bbva.fiduciaria.pos.transversales;

/**
 * Esta clase define las constantes que se utilizan a lo largo del proyecto para
 * los mensajes de error a nivel de captor y a nivel de servicio REST.
 * 
 * @author TryCore - http://www.trycore.com.co
 * @version 1.0
 *
 */
public class Constantes {
	// *************************************************************************************************************************************
	// * Errores producidos a nivel de la interface de conexión con Suprema.
	// *************************************************************************************************************************************
	public static final String TOKEN_CAPTOR_ATTRIBUTE_KEY = "TOKEN_CAPTOR_ATTRIBUTE";
	// *************************************************************************************************************************************
	// * Errores producidos a nivel de la interface de conexión con Suprema.
	// *************************************************************************************************************************************
	public static final String ERROR_ESTABLECIMIENTO_CONEXION_DISPOSITIVO = "No se pudo establecer conexión con el dispositivo.";
	public static final String ERROR_SETEO_TIEMPO_ESPERA_DETALLE = "Error seteando el tiempo de espera : ";
	public static final String ERROR_SETEO_NIVEL_LFD_DETALLE = "Error seteando el nivel LFD : ";
	public static final String ERROR_SETEO_TIPO_TEMPLATE_DETALLE = "Error seteando el tipo de template : ";
	public static final String ERROR_SETEO_NIVEL_SEGURIDAD_DETALLE = "Error seteando el nivel de seguridad : ";
	public static final String ERROR_DISPOSITIVO_NO_INICIALIZADO = "El dispositivo no ha sido inicializado.";
	public static final String ERROR_CAPTURA_HUELLA_DETALLE = "Error capturando la huella: ";
	public static final String ERROR_FORMATO_IMAGEN_DETALLE = "Error formateando la imagen: ";
	public static final String ERROR_EXTRACCION_CAPTURA = "Error extrayendo la captura";
	public static final String ERROR_EXTRACCION_TEMPLATE_CAPTURA = "Error extrayendo el template de la captura.";
	public static final String ERROR_EXTRACCION_TEMPLATE_CAPTURA_DETALLE = "Error extrayendo el template de la captura: ";
	public static final String ERROR_EXTRACCION_TEMPLATE_ENCRIPTADO_CAPTURA = "Error extrayendo el template encriptado de la captura";
	public static final String ERROR_VERIFICACION_HUELLA_DETALLE = "Error verificando la huella : ";
	public static final String ERROR_ENVIO_TEMPLATE_DETALLE = "Error enviando el template : ";

	// *************************************************************************************************************************************
	// * Errores producidos al capturar la huella desde los servicios REST expuestos
	// a fiduciaria.
	// *************************************************************************************************************************************
	public static final String ERROR_INICIALIZACION_CAPTOR_HUELLAS = "Se ha producido un error al iniciar el captor de huellas digitales.";
	public static final String ERROR_CAPTURA_HUELLAS_DIGITALES = "Se ha producido un error al capturar la huella.";
	public static final String ERROR_OBTENCION_CAPTURA_HUELLA_DIGITAL = "Se ha producido un error al obtener la captura de la huella.";
	public static final String ERROR_OBTENCION_CAPTURA_HUELLA_DIGITAL_EN_BASE_64 = "Se ha producido un error al obtener la captura de la huella codificada en Base 64.";
	public static final String ERROR_OBTENCION_TEMPLATE_HUELLA_DIGITAL = "Se ha producido un error al obtener el template de la huella capturada.";
	public static final String ERROR_OBTENCION_CALIDAD_CAPTURA_HUELLA_DIGITAL = "Se ha producido un error al obtener la calidad de la huella capturada.";
	public static final String ERROR_VERIFICACION_CAPTURA_HUELLA_DIGITAL = "Se ha producido un error al verificar la captura de la huella.";
	public static final String ERROR_GENERACION_IMAGEN_HUELLA = "Se ha producido un error al generar la imagen de la huella capturada.";
}
